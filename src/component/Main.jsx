import React from 'react';
import ItemContext from './ItemContext';

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
    }
  }
  
  componentDidMount() {
    fetch("https://api.github.com/events")
      .then(response => response.json())
      .then(
        result => {
          this.setState({items: result});
          result.forEach(element => {
            localStorage.setItem(element.id, JSON.stringify(element));
          });
        },
        error => {
        }
      );
  }

  render() {
    return (
      <ItemContext.Provider value={this.state.items}>
        <div className="main">
          {this.props.children}
        </div>
      </ItemContext.Provider>
    )
  }
}
