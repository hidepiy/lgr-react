import React from 'react';
import './Item.css';

const Item = ({ id, type, actor, created_at }) => {
  return (
    <div className="item">
          <div className="item-main">
            <div>
              <img
                className="avatar"
                src={actor.avatar_url}
                alt="{actor.display_login}"
              />
            </div>
            <div>
              <p className="display-login">
                <span>{actor.display_login}</span>
              </p>
              <p className="event">
                <span>{type}</span>
              </p>
            </div>
          </div>
          <div>
            <p className="created-at">
              {created_at}
            </p>
          </div>
        </div>
  )
}

export default Item;