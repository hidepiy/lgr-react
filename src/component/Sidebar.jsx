import React, { useState, useRef } from 'react';
import './Sidebar.css';
import { useOnClickOutside } from '../hooks';
import { Burger } from '.';

const Sidebar = () => {
  const [open, setOpen] = useState(false);
  const node = useRef();
  useOnClickOutside(node, () => setOpen(false))

  
  const isHidden = open ? true : false;
  const tabIndex = isHidden ? 0 : -1;

  var style = {
    transform: open ? 'translateX(0)' : 'translateX(-100%)',
  }

  return (
    <div ref={node} >   
    <Burger open={open} setOpen={setOpen} ref={node} />
    <div className="sidebar" open={open} style={style} ref={node} >
    
      <a href="/" tabIndex={tabIndex}>
        <span aria-hidden="true">💁🏻‍♂️</span>
        About us
      </a>
      <a href="/" tabIndex={tabIndex}>
        <span aria-hidden="true">💸</span>
        Pricing
        </a>
      <a href="/" tabIndex={tabIndex}>
        <span aria-hidden="true">📩</span>
        Contact
      </a> 
    </div>
    </div>
    
  )
}

/* Sidebar.propTypes = {
  open: bool.isRequired,
} */

export default Sidebar;
