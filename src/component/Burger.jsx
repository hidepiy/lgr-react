import React from 'react';
import { bool, func } from 'prop-types';
import './Burger.css';

const Burger = ({ open, setOpen, ...props }) => {
  
  var style = {
/*     background: open ? '#0D0C1D' : '#EFFFFA', */
    first: {
      background: open ? '#0D0C1D' : '#0D0C1D',
      transform: open ? 'rotate(45deg)' : 'rotate(0)',
    },
    second: {
      background: open ? '#0D0C1D' : '#0D0C1D',
      opacity: open ? '0' : '1',
      transform: open ? 'translateX(20px)' : 'translateX(0)',
    },
    third: {
      background: open ? '#0D0C1D' : '#0D0C1D',
      transform: open ? 'rotate(-45deg)' : 'rotate(0)',
    }
  }

  return (
    <div className="burger" open={open} onClick={() => setOpen(!open)}>
      <span style={style.first} />
      <span style={style.second} />
      <span style={style.third} />
    </div>
  )
}

Burger.propTypes = {
  open: bool.isRequired,
  setOpen: func.isRequired,
};

export default Burger;
