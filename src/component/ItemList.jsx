import React from 'react';
import ItemContext from './ItemContext';
import { Item } from '.';

const ItemList = () => {
  const items = React.useContext(ItemContext);
  return(
    <div className="item-list">
    {items.map(element => ( 
      <Item
        key={element.id}
        id={element.id}
        type={element.type}
        actor={element.actor}
        created_at={element.created_at}
      />
    ))}
    </div>
  ) 
}

export default ItemList;