import React from 'react';
import './App.css';
import { Header, Main, ItemList, ItemDetail, Sidebar } from './component';

function App() {
  return (
    <div className="App">
      <div className="container">
        <div className="logo"></div>
        <Header />
        <Main>
          <ItemList />
          <ItemDetail />
        </Main>
        <footer className="footer">footer</footer> 
      </div>
      <Sidebar/>
    </div>
  );
}

export default App;
