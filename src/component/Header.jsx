import React from 'react';

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">
        <ul>
          <li>menu1</li>
          <li>menu2</li>
          <li>menu3</li>
        </ul>
      </div>
    )
  }
}
